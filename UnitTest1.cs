using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumIntro
{
    [TestFixture]
    public class Tests
    {
        private IWebDriver driver;

        [OneTimeSetUp]
        public void BasicBrowserSetUp()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
        }

        [SetUp]
        public void Setup()
        {
            driver.Navigate().GoToUrl("https://www.epam.com/");
        }

        [Test]
        public void VerifyMainElements()
        {
            var logo = driver.FindElement(By.XPath("//img[@class='header__logo']"));
            var slider = driver.FindElement(By.XPath("//span[@class='title-slider__slide-row']"));
            var joinSection = driver.FindElement(By.XPath("//div[@class='section__wrapper section--padding-large']"));

            Assert.Multiple(() =>
            {
                Assert.True(logo.Displayed);
                Assert.True(slider.Displayed);
                Assert.True(joinSection.Displayed);
            });
        } 

        [Test]
        public void SearchButton()
        {
            var searchButton = driver.FindElement(By.XPath("//button[@class='header-search__button header__icon']"));
            searchButton.Click();

            new WebDriverWait(driver, TimeSpan.FromSeconds(3))
                .Until(locationY => driver.FindElement(By.XPath("//div[@class='header-search__panel opened']")).Location.Y >= 68);

            var findButton = driver.FindElement(By.XPath("//button[@class='header-search__submit']"));
            Assert.True(findButton.Displayed);
        }

        [Test]
        public void SearchFunction()
        {
            var searchButton = driver.FindElement(By.XPath("//button[@class='header-search__button header__icon']"));
            searchButton.Click();

            new WebDriverWait(driver, TimeSpan.FromSeconds(3))
                .Until(locationY => driver.FindElement(By.XPath("//div[@class='header-search__panel opened']")).Location.Y >= 68);

            var searchInput = driver.FindElement(By.XPath("//input[@id='new_form_search']"));
            searchInput.SendKeys("Join our Team");
            var findButton = driver.FindElement(By.XPath("//button[@class='header-search__submit']"));
            findButton.Click();

            var firstResultElement = driver.FindElement(By.XPath("//a[@class='search-results__title-link']"));
            var actualTextOfFirstResultElement = firstResultElement.Text;
            var expectedTextOfFirstResultElement = "Join our Team";

            Assert.That(actualTextOfFirstResultElement.Equals(expectedTextOfFirstResultElement));
        }

        [Test]
        public void ContactPage()
        {
            var contactUsButton = driver.FindElement(By.XPath("//a[@data-gtm-category='header-contact-cta']"));
            contactUsButton.Click();

            var expectedUrl = "https://www.epam.com/about/who-we-are/contact";
            var actualUrl = driver.Url;

            Assert.That(actualUrl.Equals(expectedUrl));
        }

        [Test]
        public void LanguageChange()
        {
            var locationButton = driver.FindElement(By.XPath("//button[@class='location-selector__button']"));
            locationButton.Click();

            new WebDriverWait(driver, TimeSpan.FromSeconds(3))
                .Until(locationY => driver.FindElement(By.XPath("//strong[@class='location-selector__title']")).Location.Y >= 91);

            var ukrainianLanguageOption = driver.FindElement(By.XPath("//li[@class='location-selector__item']/a[@lang='uk']"));
            ukrainianLanguageOption.Click();

            new WebDriverWait(driver, TimeSpan.FromSeconds(3))
                .Until(title => driver.Title.Contains("Ukraine"));

            var expectedLang = "uk-UA";
            var actualLang = driver.FindElement(By.XPath("//html[@class='no-touchevents']")).GetAttribute("lang");

            Assert.That(actualLang.Equals(expectedLang));
        }

        [TearDown]
        public void WaitBetweenTests()
        {
            Thread.Sleep(1000);
        }

        [OneTimeTearDown]
        public void MyTearDown()
        {
            driver.Quit();
        }
    }
}